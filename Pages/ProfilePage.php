<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile Page</title>

    <?php
        include('../Components/fontAwesome/_fontAwesome.php');
        include('../Databases/Database.php');
    ?>
</head>
<body>
    <!-- Navbar -->
    <?php
        include('../Components/navbar/_navbar.php');
        if(!isset($_SESSION['user_role'])){
            header('location:LandingPage.php');
        }
    ?>

    <!-- Profile -->
    <?php
        include('../Components/profile/_profile.php');
    ?>

    <!-- Bootstrap CDN -->
    <?php
        include('../Components/bootstrapCDN/_bootstrapCDN.php');
    ?>
</body>
</html>