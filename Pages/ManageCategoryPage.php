<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Category Page</title>

    <?php
        include('../Components/fontAwesome/_fontAwesome.php');
        include('../Databases/Database.php');
    ?>
</head>
<body>
    <!-- Navbar -->
    <?php
        include('../Components/navbar/_navbar.php');
    ?>

    <!-- Category Header -->
    <?php
        include('../Components/header/_manageCategoryHeader.php');
        if(!isset($_SESSION['user_role']) > 2 || !isset($_SESSION['user_role'])){
            header('location:LandingPage.php');
        }
    ?>

    <!-- Category List -->
    <?php
        include('../Components/list/_manageCategoryList.php');
    ?>
    
    <!-- Bootstrap CDN -->
    <?php
        include('../Components/bootstrapCDN/_bootstrapCDN.php');
    ?>
</body>
</html>