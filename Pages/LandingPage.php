<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Just Store Landing Page</title>

    <?php
        include('../Components/fontAwesome/_fontAwesome.php');
        include('../Databases/Database.php');
    ?>
    
</head>
<body>
    
    <!-- Navbar -->
    <?php
        include('../Components/navbar/_navbar.php');
    ?>

    <!-- Carousel  -->
    <?php
        include('../Components/carousel/_carousel.php');
    ?>

    <!-- Item -->
    <?php
        include('../Components/card/_landingPageCard.php');
    ?>

    <!-- Bootstrap CDN -->
    <?php
        include('../Components/bootstrapCDN/_bootstrapCDN.php');
    ?>
</body>
</html>