$(document).ready(function() {
    let $qty_up = $(".qty-up");
    let $qty_down = $(".qty-down");
    let $input = $(".qty-input");

    $qty_up.click(function(e) {
       if($input.val() >= 1 && $input.val() < 10) {
           $input.val(function(i, oldval){
               return ++oldval;
           });
       }
    });

    $qty_down.click(function(e) {
        if($input.val() > 1 && $input.val() <= 10) {
            $input.val(function(i, oldval){
                return --oldval;
            });
        }
    });

    $(function(){
        $("input[type = 'submit']").click(function(){
           var $fileUpload = $("input[type='file']");
           if (parseInt($fileUpload.get(0).files.length) > 3){
              alert("You are only allowed to upload a maximum of 3 files");
           }
        });
    });

});