<?php
    include('Database.php');

    $sliderName = $_POST['sliderName'];
    $sliderSequence = $_POST['sliderSequence'];
    $sliderStartDate = $_POST['sliderStartDate'];
    $sliderEndDate = $_POST['sliderEndDate'];
    $sliderHyperlink = $_POST['sliderHyperlink'];
    $sliderPicture = $_FILES['sliderPicture'];

    
    if(strlen($sliderName) < 5) {
        echo 'Name to short, minimal 5 characters';
    } else if ($sliderSequence < 1) {
        echo 'Invalid Sequence';
    } else {
        $file = $sliderPicture['name'];
        $tmpPath = $sliderPicture['tmp_name'];
    
        $store = "../assets/sliderPicture/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "INSERT INTO MsSlider (`slider_name`, `slider_sequence`, `slider_start_date`, `slider_end_date`, `slider_hyperlink`, `slider_picture`) VALUES ('$sliderName','$sliderSequence','$sliderStartDate','$sliderEndDate','$sliderHyperlink','$store')";
    
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ManageSliderPage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>