<?php

    include('Database.php');

    $courierID = $_POST['courierID'];
    $courierName = $_POST['courierName'];
    $courierCost = $_POST['courierCost'];
    $courierIcon = $_FILES['courierIcon'];

    
    if(strlen($courierName) < 3) {
        echo 'Name to Short, minimal 3 characters';
    }else if ($courierCost < 5000) {
        echo 'Price to small, Minimal 5000';
    }else {
        $file = $courierIcon['name'];
        $tmpPath = $courierIcon['tmp_name'];
    
        $store = "../assets/courierIcon/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "UPDATE MsCourier SET courier_name='$courierName', courier_cost='$courierCost', courier_icon='$store', courier_last_update=current_timestamp WHERE courier_id='$courierID'";
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ManageCourierPage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>