<?php

    include('Database.php');

    $ProductID = $_POST['productID'];
    $ProductName = $_POST['productName'];
    $ProductDescription = $_POST['productDescription'];
    $ProductPrice = $_POST['productPrice'];
    $ProductStock = $_POST['productStock'];
    $ProductPicture = $_FILES['productPicture'];

    
    if(strlen($ProductName) < 3) {
        echo 'Name to Short, minimal 3 characters';
    } else if (strlen($ProductDescription) < 10 ) {
        echo 'Description to short, minimal 10 characters';
    } else if ($ProductPrice < 10000) {
        echo 'Price to small, minimal 10000';
    } else if ($ProductStock < 0) {
        echo 'Invalid Stock input, minimal 0 stock';
    } else {
        $file = $ProductPicture['name'];
        $tmpPath = $ProductPicture['tmp_name'];
    
        $store = "../assets/productPicture/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "UPDATE MsProduct SET product_name='$ProductName', product_description='$ProductDescription', product_price='$ProductPrice', product_stock='$ProductStock', product_picture='$store' WHERE product_id='$ProductID'";
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ManageProductPage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>