<?php
    session_start();
    include('Database.php');

    $userID = $_SESSION['user_id'];
    $userName = $_POST['username'];
    $userAddress = $_POST['address'];
    $userPhone = $_POST['phone'];
    $userGender = $_POST['gender'];
    $userEmail = $_POST['email'];
    $userPicture = $_FILES['profile'];

    $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

    if(strlen($userName) < 3) {
        echo 'Name to Short, minimal 3 characters';
    } else if (strlen($userAddress) < 10) {
        echo 'Address to Short, minimal 10 characters';
    } else if (!preg_match($pattern, $userEmail)) {
        echo 'Invalid email format';
    } else {
        $file = $userPicture['name'];
        $tmpPath = $userPicture['tmp_name'];
    
        $store = "../assets/profilePicture/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "UPDATE MsUser SET `username`='$userName', user_address='$userAddress', user_phone='$userPhone', user_gender='$userGender', user_email='$userEmail', user_profile_picture='$store' WHERE `user_id`='$userID'";
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ProfilePage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>