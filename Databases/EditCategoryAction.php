<?php

    include('Database.php');

    $CategoryID = $_POST['CategoryID'];
    $CategoryName = $_POST['CategoryName'];
    $CategoryIcon = $_FILES['CategoryIcon'];

    
    if(strlen($CategoryName) < 3) {
        echo 'Name to Short, minimal 3 characters';
    } else {
        $file = $CategoryIcon['name'];
        $tmpPath = $CategoryIcon['tmp_name'];
    
        $store = "../assets/categoryIcon/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "UPDATE MsCategory SET category_name='$CategoryName', category_icon='$store', category_last_update=current_timestamp WHERE category_id='$CategoryID'";
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ManageCategoryPage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>