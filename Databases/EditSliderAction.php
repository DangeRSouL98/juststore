<?php

    include('Database.php');

    $sliderID = $_POST['sliderID'];
    $sliderName = $_POST['sliderName'];
    $sliderSequence = $_POST['sliderSequence'];
    $sliderStartDate = $_POST['sliderStartDate'];
    $sliderEndDate = $_POST['sliderEndDate'];
    $sliderHyperlink = $_POST['sliderHyperlink'];
    $sliderPicture = $_FILES['sliderPicture'];

    
    if(strlen($sliderName) < 5) {
        echo 'Name to short, minimal 5 characters';
    } else if ($sliderSequence < 1) {
        echo 'Invalid Sequence';
    } else {
        $file = $sliderPicture['name'];
        $tmpPath = $sliderPicture['tmp_name'];
    
        $store = "../assets/sliderPicture/".$file;
        move_uploaded_file($tmpPath, $store);
    
        $query = "UPDATE MsSlider SET slider_name='$sliderName', slider_sequence='$sliderSequence', slider_start_date='$sliderStartDate', slider_end_date='$sliderEndDate', slider_hyperlink='$sliderHyperlink', slider_picture='$store' WHERE slider_id='$sliderID'";
        $result = mysqli_query($conn, $query);
        
        if ($result) {
            header("location:../Pages/ManageSliderPage.php");
        } else {
            echo mysqli_error($conn);
        }
    }
?>