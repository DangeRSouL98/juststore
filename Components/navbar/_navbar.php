<?php
    session_start();
?>



<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../Pages/LandingPage.php">JustStore</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php 
                if(!isset($_SESSION['username'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../Pages/LoginPage.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Pages/RegisterPage.php">Register</a>
                    </li> <?php
                } else {
                    if($_SESSION['user_role'] == 3){
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="../Pages/ProfilePage.php"><?php echo ($_SESSION['username']) ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../Pages/ShoppingHistoryPage.php">History</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../Pages/CartPage.php">Cart</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../Databases/LogoutAction.php">Logout</a>
                            </li>
                        <?php
                    }else if($_SESSION['user_role'] == 2){ 
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ProfilePage.php"><?php echo ($_SESSION['username']) ?></a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageCourierPage.php">Manage Courier</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageProductPage.php">Manage Product</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageCategoryPage.php">Manage Category</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Databases/LogoutAction.php">Logout</a>
                        </li>
                        <?php
                    }else if($_SESSION['user_role'] == 1){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ProfilePage.php"><?php echo ($_SESSION['username']) ?></a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageCourierPage.php">Manage Courier</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageProductPage.php">Manage Product</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageCategoryPage.php">Manage Category</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageSliderPage.php">Manage Slider</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Pages/ManageStaffAdminPage.php">Manage Staff/Admin</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="../Databases/LogoutAction.php">Logout</a>
                        </li>
                        <?php
                    }
                }

            ?>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>