<section id="courier" class="mx-3 my-3">
    <div class="row py-3">
        <div class="col-sm-2"><h4>Name</h4></div>
        <div class="col-sm-2"><h4>Cost</h4></div>
        <div class="col-sm-6"><h4>Icon</h4></div>
        <div class="col-sm-2"><h4>Action</h4></div>
    </div>
    <div class="row border-top border-bottom py-3">
        <?php
            include('../Databases/Database.php');

            $sql = "SELECT courier_id, courier_name, courier_cost, courier_icon, courier_last_update FROM MsCourier";
            $result = $conn->query($sql) or die($conn->error);

            while($row=$result->fetch_assoc()){
                echo '<div class="col-sm-2">'.$row['courier_name'].'</div>';
                echo '<div class="col-sm-2">'.$row['courier_cost'].'</div>';
                echo '<div class="col-sm-6"><img src="'.$row['courier_icon'].'" alt="" width="10%">'.$row['courier_icon'].'</div>';
                echo '
                    <div class="col-sm-2">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageCourierListModal'.$row['courier_id'].'">
                            <i class="fas fa-pen"></i>
                        </button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="manageCourierListModal'.$row['courier_id'].'" tabindex="-1" role="dialog" aria-labelledby="manageCourierListModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Courier</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="../Databases/EditCourierAction.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="courierID">ID</label>
                                                <span>'.$row['courier_id'].'</span>
                                                <input type="hidden" name="courierID" value="'.$row['courier_id'].'">
                                            </div>
                                            <div class="form-group">
                                                <label for="courierName">Name</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="courierName" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="courierCost">Cost</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="courierCost" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="courierIcon">Icon</label>
                                                <input type="file" class="form-control-file" name="courierIcon" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="courierLastUpdate">Last Update :</label>
                                                <span>'.$row['courier_last_update'].'</span>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-secondary">Edit Courier</button>
                                            </div>
                                        </form>     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="../Databases/DeleteCourierAction.php" method="post">
                            <input type="hidden" name="courierID" value="'.$row['courier_id'].'">
                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </div>
                ';
            }
        ?>
    </div>
</section>