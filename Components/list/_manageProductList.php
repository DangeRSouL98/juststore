<section id="courier" class="mx-3 my-3">
    <div class="row py-3">
        <div class="col-sm-6"><h4>Name</h4></div>
        <div class="col-sm-2"><h4>Price</h4></div>
        <div class="col-sm-2"><h4>Stock</h4></div>
        <div class="col-sm-2"><h4>Action</h4></div>
    </div>
    <div class="row border-top border-bottom py-3">
        <?php
            include('../Databases/Database.php');
            
            $sql = "SELECT product_id, product_name, product_description, product_price, product_stock, product_picture, b.category_id, category_name FROM MsProduct a JOIN MsCategory b ON a.category_id = b.category_id";
            $result = $conn->query($sql) or die($conn->error);

            while($row=$result->fetch_assoc()){
                echo '<div class="col-sm-6"><img src="'.$row['product_picture'].'" alt="" width="10%">'.$row['product_name'].'</div>';
                echo '<div class="col-sm-2">Rp. '.$row['product_price'].'</div>';
                echo '<div class="col-sm-2">'.$row['product_stock'].'</div>';
                echo '
                    <div class="col-sm-2">

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageProductListModal'.$row['product_id'].'">
                            <i class="fas fa-pen"></i>
                        </button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="manageProductListModal'.$row['product_id'].'" tabindex="-1" role="dialog" aria-labelledby="manageProductListModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Product</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="../Databases/EditProductAction.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="productName">Name</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="productName" required>
                                                <input type="hidden" name="productID" value="'.$row['product_id'].'">
                                            </div>
                                            <div class="form-group">
                                                <label for="productDescription">Description</label>
                                                <textarea class="form-control" rows="3" name="productDescription" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="productPrice">Price</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="productPrice" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="productStock">Stock</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="productStock" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="productPicture">Picture</label>
                                                <input type="file" class="form-control-file" name="productPicture" required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-secondary">Save Change</button>
                                            </div>
                                        </form>     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="../Databases/DeleteProductAction.php" method="post">
                            <input type="hidden" name="productID" value="'.$row['product_id'].'">
                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </div>
                ';
            }
        ?>
    </div>
</section>