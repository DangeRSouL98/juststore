<section id="courier" class="mx-3 my-3">
    <div class="row py-3">
        <div class="col-sm-4"><h4>Name</h4></div>
        <div class="col-sm-6"><h4>Icon</h4></div>
        <div class="col-sm-2"><h4>Action</h4></div>
    </div>
    <div class="row border-top border-bottom py-3">
        <?php
            include('../Databases/Database.php');

            $sql = "SELECT category_id, category_name, category_icon, category_last_update FROM MsCategory";
            $result = $conn->query($sql) or die($conn->error);

                while($row=$result->fetch_assoc()){
                    echo '<div class="col-sm-4">'.$row['category_name'].'</div>';
                    echo '<div class="col-sm-6"><img src="'.$row['category_icon'].'" alt="" width="10%">'.$row['category_icon'].'</div>';
                    echo '
                        <div class="col-sm-2">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageCategoryListModal'.$row['category_id'].'">
                                <i class="fas fa-pen"></i>
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="manageCategoryListModal'.$row['category_id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Category</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="../Databases/EditCategoryAction.php" method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="categoryID">ID</label>
                                                    <span>'.$row['category_id'].'</span>
                                                    <input type="hidden" name="CategoryID" value="'.$row['category_id'].'">
                                                </div>
                                                <div class="form-group">
                                                    <label for="categoryName">Name</label>
                                                    <input class="form-control" type="text" placeholder="'.$row['category_name'].'" name="CategoryName" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="categoryIcon">Icon</label>
                                                    <input type="file" class="form-control-file" name="CategoryIcon" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleFormControlFile1">Last Update :</label>
                                                    <span>'.$row['category_last_update'].'</span>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-secondary">Edit Category</button>
                                                </div>
                                            </form>     
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="../Databases/DeleteCategoryAction.php" method="post">
                                <input type="hidden" name="CategoryID" value="'.$row['category_id'].'">
                                <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                            </form>
                        </div>
                    ';
                }
        ?>
    </div>
</section>