<?php

    $query = "SELECT * FROM MsUser";
    $result = mysqli_query($conn, $query);
    
    $row=$result->fetch_assoc()
?>


<section class="mx-3 my-3">
    <div class="row py-3">
        <div class="col-sm-4"><h4>Name</h4></div>
        <div class="col-sm-1"><h4>Gender</h4></div>
        <div class="col-sm-2"><h4>Phone</h4></div>
        <div class="col-sm-3"><h4>Email</h4></div>
        <div class="col-sm-2"><h4>Action</h4></div>
    </div>
    <div class="row border-top border-bottom py-3">
        <?php
            while($row=$result->fetch_assoc()){ 
                if ($row['user_role'] < 3) {?>
                <div class="col-sm-4"><?php echo $row['username'] ?></div> 
                <div class="col-sm-1"><?php echo $row['user_gender'] ?></div> 
                <div class="col-sm-2"><?php echo $row['user_phone'] ?></div> 
                <div class="col-sm-3"><?php echo $row['user_email'] ?></div> 
                <div class="col-sm-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#EditStaffAdminModal<?php echo $row['user_id'] ?>">
                        <i class="fas fa-pen"></i>
                    </button>               
                    <!-- Modal -->
                    <div class="modal fade" id="EditStaffAdminModal<?php echo $row['user_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit <?php 
                                        if($row['user_role'] == 2) {
                                            echo 'Staff';
                                        }else if ($row['user_role'] == 1) {
                                            echo 'Admin';
                                        } ?> Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="../Databases/EditStaffAdminInformationAction.php" method="post">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input class="form-control" type="text" placeholder=<?php echo $row['username'] ?> name="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="gender">Gender:</label><br>
                                            <input type="radio" name="gender" id="female"<?php if (isset($gender) && $gender=="female")?> value="female" checked>Female
                                            <input type="radio" name="gender" id="male"<?php if (isset($gender) && $gender=="male")  ?> value="male" >Male
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <textarea class="form-control" name="address" rows="3" placeholder=<?php echo $row['user_address'] ?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone</label>
                                            <input class="form-control" type="text" name="phone" placeholder=<?php echo $row['user_phone'] ?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" aria-describedby="emailHelp" placeholder=<?php echo $row['user_email'] ?> name="email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Re-enter Password</label>
                                            <input type="password" class="form-control" name="repassword" required>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="hidden" value=<?php echo ($row['user_id']) ?> name="uid">
                                            <button type="submit" class="btn btn-secondary">Save</button>
                                        </div>
                                    </form>     
                                </div>
                            </div>
                        </div>
                    </div>              
                    <form action="../Databases/DeleteStaffAdminInformationAction.php" method="post">
                        <input type="hidden" value=<?php echo ($row['user_id']) ?> name="uid">
                        <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                    </form>
                </div> 
                
                
                
                <?php }

            }
        ?>
    </div>
</section>