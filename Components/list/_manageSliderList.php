<section id="courier" class="mx-3 my-3">
    <div class="row py-3">
        <div class="col-sm-1"><h4>Sequence</h4></div>
        <div class="col-sm-2"><h4>Name</h4></div>
        <div class="col-sm-2"><h4>Image</h4></div>
        <div class="col-sm-3"><h4>Hyperlink</h4></div>
        <div class="col-sm-1"><h4>Start At</h4></div>
        <div class="col-sm-1"><h4>End At</h4></div>
        <div class="col-sm-2"><h4>Action</h4></div>
    </div>

    <div class="row border-top border-bottom py-3">

        <?php
            include('../Databases/Database.php');

            $sql = "SELECT slider_id, slider_name, slider_sequence, slider_start_date, slider_end_date, slider_hyperlink, slider_picture FROM MsSlider";
            $result = $conn->query($sql) or die($conn->error);

            while($row=$result->fetch_assoc()){
                echo '<div class="col-sm-1">'.$row['slider_sequence'].'</div>';
                echo '<div class="col-sm-2">'.$row['slider_name'].'</div>';
                echo '<div class="col-sm-2"><img src="'.$row['slider_picture'].'" alt="" width="20%">'.$row['slider_picture'].'</div>';
                echo '<div class="col-sm-3">'.$row['slider_hyperlink'].'</div>';
                echo '<div class="col-sm-1">'.$row['slider_start_date'].'</div>';
                echo '<div class="col-sm-1">'.$row['slider_end_date'].'</div>';
                echo '
                    <div class="col-sm-2">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageSliderListModal'.$row['slider_id'].'">
                            <i class="fas fa-pen"></i>
                        </button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="manageSliderListModal'.$row['slider_id'].'" tabindex="-1" role="dialog" aria-labelledby="manageSliderListModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Slider</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="../Databases/EditSliderAction.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="sliderID">ID</label>
                                                <span>'.$row['slider_id'].'</span>
                                                <input type="hidden" name="sliderID" value="'.$row['slider_id'].'">
                                            </div>
                                            <div class="form-group">
                                                <label for="sliderName">Name</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="sliderName" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="sliderSequence">Sequence</label>
                                                <input class="form-control" type="text" placeholder="Default input" name="sliderSequence" required>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-date-input" class="col-2 col-form-label">Start Date</label>
                                                <div class="col-10">
                                                    <input class="form-control" type="date" name="sliderStartDate" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-date-input" class="col-2 col-form-label">End Date</label>
                                                <div class="col-10">
                                                    <input class="form-control" type="date" name="sliderEndDate" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="sliderHyperlink">Hyperlink</label>
                                                <textarea class="form-control" name="sliderHyperlink" rows="3" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="sliderPicture">Picture</label>
                                                <input type="file" class="form-control-file" name="sliderPicture" required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-secondary">Save Slider</button>
                                            </div>
                                        </form>     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="../Databases/DeleteSliderAction.php" method="post">
                            <input type="hidden" name="sliderID" value="'.$row['slider_id'].'">
                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </div>
                ';
            }
        ?>
    </div>
</section>