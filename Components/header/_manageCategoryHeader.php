<div>
    <h3 class="mx-3 my-3">Category</h3>
    <form class="form-inline">
        <input class="form-control mr-sm-2 mx-3" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageCategoryHeaderModal">
        Add
    </button>
    
    <!-- Modal -->
    <div class="modal fade" id="manageCategoryHeaderModal" tabindex="-1" role="dialog" aria-labelledby="manageCategoryHeaderModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="../Databases/AddNewCategoryAction.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="CategoryName">Name</label>
                            <input class="form-control" type="text" placeholder="Category Name" name="CategoryName" required>
                        </div>
                        <div class="form-group">
                            <label for="CategoryIcon">Icon</label>
                            <input type="file" class="form-control-file" name="CategoryIcon" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Add Category</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>