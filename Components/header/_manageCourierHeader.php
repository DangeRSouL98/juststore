<div>
    <h3 class="mx-3 my-3">Courier</h3>
    <form class="form-inline">
        <input class="form-control mr-sm-2 mx-3" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageCourierHeaderModal">
        Add
    </button>
    

    <!-- Modal -->
    <div class="modal fade" id="manageCourierHeaderModal" tabindex="-1" role="dialog" aria-labelledby="manageCourierHeaderModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Courier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="../Databases/AddNewCourierAction.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="courierName">Name</label>
                            <input class="form-control" type="text" placeholder="Default input" name="courierName" required>
                        </div>
                        <div class="form-group">
                            <label for="courierCost">Cost</label>
                            <input class="form-control" type="text" placeholder="Default input" name="courierCost" required>
                        </div>
                        <div class="form-group">
                            <label for="courierIcon">Icon</label>
                            <input type="file" class="form-control-file" name="courierIcon" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-secondary">Add Courier</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>