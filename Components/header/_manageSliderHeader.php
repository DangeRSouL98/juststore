<div>
    <h3 class="mx-3 my-3">Home Slider</h3>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageSliderHeaderModal">
        Add
    </button>
    
    <!-- Modal -->
    <div class="modal fade" id="manageSliderHeaderModal" tabindex="-1" role="dialog" aria-labelledby="manageSliderHeaderModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Slider</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="../Databases/AddNewSliderAction.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="sliderName">Name</label>
                            <input class="form-control" type="text" placeholder="Default input" name="sliderName" required>
                        </div>
                        <div class="form-group">
                            <label for="sliderSequence">Sequence</label>
                            <input class="form-control" type="text" placeholder="Default input" name="sliderSequence" required>
                        </div>
                        <div class="form-group row">
                            <label for="sliderStartDate" class="col-2 col-form-label">Start Date</label>
                            <div class="col-10">
                                <input class="form-control" type="date" name="sliderStartDate" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sliderEndDate" class="col-2 col-form-label">End Date</label>
                            <div class="col-10">
                                <input class="form-control" type="date" name="sliderEndDate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sliderHyperlink">Hyperlink</label>
                            <textarea class="form-control" name="sliderHyperlink" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="sliderPicture">Picture</label>
                            <input type="file" class="form-control-file" name="sliderPicture" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-secondary">Add Slider</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>