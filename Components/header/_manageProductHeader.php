<div>
    <h3 class="mx-3 my-3">Product</h3>
    <form class="form-inline">
        <input class="form-control mr-sm-2 mx-3" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-warning mx-3 my-3" data-toggle="modal" data-target="#manageProductHeaderModal">
        Add
    </button>
    
    <!-- Modal -->
    <div class="modal fade" id="manageProductHeaderModal" tabindex="-1" role="dialog" aria-labelledby="manageProductHeaderModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="../Databases/AddNewProductAction.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="productName">Name</label>
                            <input class="form-control" type="text" placeholder="Default input" name="productName" required>
                        </div>
                        <div class="form-group">
                            <label for="productDescription">Description</label>
                            <textarea class="form-control" rows="3" name="productDescription" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="productPrice">Price</label>
                            <input class="form-control" type="text" placeholder="Default input" name="productPrice" required>
                        </div>
                        <div class="form-group">
                            <label for="productStock">Stock</label>
                            <input class="form-control" type="text" placeholder="Default input" name="productStock" required>
                        </div>
                        <div class="form_group">
                            <label for="productCategory">Category</label>
                            <select class="form-control" name="productCategory">
                                <?php
                                    include('../Databases/Database.php');

                                    $sql = "SELECT category_id, category_name FROM MsCategory";
                                    $result = $conn->query($sql) or die($conn->error);

                                    while($row=$result->fetch_assoc()){
                                        echo '<option value="'.$row['category_id'].'">'.$row['category_name'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="productPicture">Picture</label>
                            <input type="file" class="form-control-file" name="productPicture" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-secondary">Submit</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>