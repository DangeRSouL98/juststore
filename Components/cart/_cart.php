<?php
    include('../Databases/Database.php');
    $query = "SELECT * FROM MsCart a JOIN MsProduct b WHERE a.product_id = b.product_id";
    $result = $conn->query($query) or die($conn->error);
    $total = 0;
?>

<section id="cart">
    <div class="row">
        <div class="col-sm-8">
            <h4>Name</h4>
        </div>
        <div class="col-sm-1">
            <h4>Price</h4>
        </div>
        <div class="col-sm-1">
            <h4>Qty</h4>
        </div>
        <div class="col-sm-1">
            <h4>Total</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form action="../Databases/DeleteCartItemAction.php" class="col-sm-12" method="post">
                <div class="row border-top border-bottom py-3 px-3">
                    <?php while($row=$result->fetch_assoc()){ ?>
                            <div class="col-sm-2">
                                <img src=<?php echo ($row['product_picture']) ?> alt="" width="100%">
                            </div> 
                            <div class="col-sm-6">
                                <h5><?php echo ($row['product_name']) ?></h5>
                            </div>
                            <div class="col-sm-1">
                                <h5><?php echo ($row['product_price']) ?></h5>
                            </div>
                            <div class="col-sm-1">
                                <h5><?php echo ($row['cart_qty']) ?></h5>
                            </div>
                            <div class="col-sm-1">
                                <h5><?php echo ($row['cart_qty'] * $row['product_price']); 
                                $total = $total + ($row['cart_qty'] * $row['product_price']);?></h5>
                            </div>
                            <div class="col-sm-1">
                                <input type="hidden" name="CartID" value=<?php echo ($row['cart_id']) ?>>
                                <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                            </div>
                            <?php
                        }
                    ?>
                </div>
            </form>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-11 text-right">
            <h5>Grand Total = Rp.
                <?php
                    echo $total;
                ?>
            </h5>
        </div>
        <form action="" method="">
            <button type="button" class="btn btn-success">Checkout</button>
        </form>
    </div>
</section>