<div class="card-group">
    <?php
        include('../Databases/Database.php');

        $query = "SELECT * FROM MsProduct";
        $result = mysqli_query($conn, $query);

        while($row=$result->fetch_assoc()){
            echo '<div class="card">';
            echo '<img src="'.$row['product_picture'].'" class="card-img-top" alt="...">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$row['product_name'].'</h5>';
            echo '<p class="card-text">Price : '.$row['product_price'].'</p>';
            echo '<p class="card-text">Rating : '.$row['product_rating'].'</p>';
            echo '<a href="../Pages/ProductDetailPage.php?product_id='.$row['product_id'].'"><button type="submit" class="btn btn-primary">Info</button></a>';
            echo '</div></div>';
        }
    ?>
</div>