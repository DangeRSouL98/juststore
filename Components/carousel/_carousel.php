<?php
    include('../Databases/Database.php');

    function make_query($conn){
        $query = "SELECT * FROM MsSlider ORDER BY slider_sequence ASC";
        $result = mysqli_query($conn, $query);

        return $result;
    }

    function make_slides($conn) {
        $output = '';
        $count = 0;
        $result = make_query($conn);
        while($row = mysqli_fetch_array($result)) {
            if($count == 0) {
                $output .= '<div class="carousel-item active" data-interval="10000">';
            } else {
                $output .= '<div class="carousel-item" data-interval="10000">';
            }
            $output .= '
                <img src="'.$row["slider_picture"].'" class="d-block w-100%" alt="..."/>
                </div>
            ';
            $count = $count + 1;
        }
        return $output;
    }
?>

<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
        <?php echo make_slides($conn); ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div>