<h1>Admin/Staff Information</h1>
<form>
    <div class="form-group">
        <label for="exampleInputEmail1">Username</label>
        <input class="form-control" type="text" placeholder="Default input">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Gender :</label>
        <div class="custom-control custom-radio">
            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="customRadio1">Male</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="customRadio2">Female</label>
        </div>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Address</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Phone</label>
        <input class="form-control" type="text" placeholder="Default input">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Re-enter Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1">
    </div>
    <button type="submit" class="btn btn-primary">Save Changes</button>
</form>