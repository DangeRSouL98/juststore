<form action="../Databases/RegisterAction.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="username">Username</label>
        <input class="form-control" type="text" placeholder="Default input" name="username" required>
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <textarea class="form-control" name="address" rows="3" required></textarea>
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input class="form-control" type="text" placeholder="Default input" name="phone" required>
    </div>
    <div class="form-group">
    <div class="form-group">
        <label for="gender">Gender:</label><br>
        <input type="radio" name="gender" id="female"<?php if (isset($gender) && $gender=="female")?> value="female" checked>Female
        <input type="radio" name="gender" id="male"<?php if (isset($gender) && $gender=="male")  ?> value="male" >Male
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" aria-describedby="emailHelp" name="email" required>
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Re-enter Password</label>
        <input type="password" class="form-control" name="repassword" required>
    </div>
    <div class="form-group">
        <label for="profilePicture">Profile Picture</label>
        <input type="file" class="form-control-file" name="profilePicture" required>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>