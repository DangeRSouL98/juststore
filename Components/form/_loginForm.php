<form action="../Databases/LoginAction.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" aria-describedby="emailHelp" name="email" required>
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" required>
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="rememberMe">
        <label class="form-check-label" for="exampleCheck1">Remember Me</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="button" class="btn btn-link">Forgot Password</button>
</form>