
<?php 
    $id=$_GET['product_id'];
    
    $query = "SELECT * FROM MsProduct WHERE product_id = '$id'";
    $result = mysqli_query($conn, $query);
    
    $row=$result->fetch_assoc()
?>

<form action="../Databases/ProductAction.php" method="post">

<div class="media">
    <img src=<?php echo '"'.$row['product_picture'].'"';  ?> class="align-self-start mr-3" alt="..." width="40%">
    <div class="media-body">
        <h4 class="mt-0"><?php echo ''.$row['product_name'].'';  ?></h4>
        <p>Item description :</p>
        <p><?php echo ''.$row['product_description'].'';  ?></p>
        <div class="list-group list-group-horizontal-xl">
            <div class="list-group-item">
                Rating :
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                <i class="fas fa-star"></i>
            </div>
            <div class="list-group-item">Price : <?php echo ''.$row['product_price'].'';  ?></div>
            <div class="list-group-item">
                Qty :
                <button class="qty-up border bg-light" type="button"><i class="fas fa-angle-up"></i></button>
                <input type="text" class="qty-input border px-2 w-50 bg-light" name="quantity" value="1" placeholder="1">
                <button class="qty-down border bg-light" type="button"><i class="fas fa-angle-down"></i></button>
            </div>
        </div>
        <input type="hidden" value=<?php echo ($id) ?> name="product_id">
        <?php
            if(!isset($_SESSION['username'])){ ?>
                <a href="../Pages/LandingPage.php"><button type="button" class="btn btn-primary my-2" >Add To Cart</button></a> <?php
            } else { ?>
                <button type="submit" class="btn btn-primary my-2" >Add To Cart</button> <?php
            } 
        ?>
    </div>
</div>

</form>