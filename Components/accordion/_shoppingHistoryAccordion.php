<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <div>
                        Transaction ID
                    </div>
                    <div>
                        Transaction Date
                    </div>
                </button>
            </h5>
        </div>
    
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div>Courier Name</div>    
                <div>Courier Cost</div>    
                <div>Address</div>    
                <div>Phone</div>    
                <div>Notes</div>    
            </div>

            <section class="card-body">
                <div class="row border-top py-3">
                    <div class="col-sm-8">
                        <h4>Name</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Qty</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Price</h4>
                    </div>
                    <div class="col-sm-2">
                        <h4>Review</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row border-top border-bottom py-3 px-3">
                            <div class="col-sm-2">
                                <img src="./assets/productImages/307791.jpg" alt="" width="100%">
                            </div>
                            <div class="col-sm-6">
                                <h5>Product Name</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Price</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Qty</h5>
                            </div>
                            <div class="col-sm-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter">
                                    Give Review
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Rating & Review</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="form-group col-md-4">
                                                        <label for="rating">Rating</label>
                                                        <select id="rating" class="form-control">
                                                            <option selected>5</option>
                                                            <option>4</option>
                                                            <option>3</option>
                                                            <option>2</option>
                                                            <option>1</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlTextarea1">Review</label>
                                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                    </div>
                                                </form>     
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Give Review</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11 text-right">
                        <h5>Grand Total = Rp.100.000</h5>
                    </div>
                </div>
            </section>
        
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <div>
                        Transaction ID
                    </div>
                    <div>
                        Transaction Date
                    </div>
                </button>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            
            <div class="card-body">
                <div>Courier Name</div>    
                <div>Courier Cost</div>    
                <div>Address</div>    
                <div>Phone</div>    
                <div>Notes</div>    
            </div>

            <section class="card-body">
                <div class="row border-top py-3">
                    <div class="col-sm-8">
                        <h4>Name</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Qty</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Price</h4>
                    </div>
                    <div class="col-sm-2">
                        <h4>Review</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row border-top border-bottom py-3 px-3">
                            <div class="col-sm-2">
                                <img src="./assets/productImages/307791.jpg" alt="" width="100%">
                            </div>
                            <div class="col-sm-6">
                                <h5>Product Name</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Price</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Qty</h5>
                            </div>
                            <div class="col-sm-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter">
                                    Give Review
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Rating & Review</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="form-group col-md-4">
                                                        <label for="rating">Rating</label>
                                                        <select id="rating" class="form-control">
                                                            <option selected>5</option>
                                                            <option>4</option>
                                                            <option>3</option>
                                                            <option>2</option>
                                                            <option>1</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlTextarea1">Review</label>
                                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                    </div>
                                                </form>     
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Give Review</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11 text-right">
                        <h5>Grand Total = Rp.100.000</h5>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingThree">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <div>
                        Transaction ID
                    </div>
                    <div>
                        Transaction Date
                    </div>
                </button>
            </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            
            <div class="card-body">
                <div>Courier Name</div>    
                <div>Courier Cost</div>    
                <div>Address</div>    
                <div>Phone</div>    
                <div>Notes</div>    
            </div>

            <section class="card-body">
                <div class="row border-top py-3">
                    <div class="col-sm-8">
                        <h4>Name</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Qty</h4>
                    </div>
                    <div class="col-sm-1">
                        <h4>Price</h4>
                    </div>
                    <div class="col-sm-2">
                        <h4>Review</h4>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row border-top border-bottom py-3 px-3">
                            <div class="col-sm-2">
                                <img src="./assets/productImages/307791.jpg" alt="" width="100%">
                            </div>
                            <div class="col-sm-6">
                                <h5>Product Name</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Price</h5>
                            </div>
                            <div class="col-sm-1">
                                <h5>Product Qty</h5>
                            </div>
                            <div class="col-sm-2">                       
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter">
                                    Give Review
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Rating & Review</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="form-group col-md-4">
                                                        <label for="rating">Rating</label>
                                                        <select id="rating" class="form-control">
                                                            <option selected>5</option>
                                                            <option>4</option>
                                                            <option>3</option>
                                                            <option>2</option>
                                                            <option>1</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlTextarea1">Review</label>
                                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                    </div>
                                                </form>     
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Give Review</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11 text-right">
                        <h5>Grand Total = Rp.100.000</h5>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>