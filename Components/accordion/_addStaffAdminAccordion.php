<h1>Add Staff/Admin</h1>
<div class="accordion" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                New Account
            </button>
            </h2>
        </div>
    
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <form action="../Databases/AddNewStaffAdminAction.php" method="post">
                    <div class="form-group">
                        <label for="role">Role:</label>
                        <select class="form-control" name="role">
                            <option value="1">Admin</option>
                            <option value="2">Staff</option>
                            <option value="3">Customer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" placeholder="Default input" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender:</label><br>
                        <input type="radio" name="gender" id="female"<?php if (isset($gender) && $gender=="female")?> value="female" checked>Female
                        <input type="radio" name="gender" id="male"<?php if (isset($gender) && $gender=="male")  ?> value="male" >Male
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Address</label>
                        <textarea class="form-control" rows="3" name="address" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input class="form-control" type="text" placeholder="Default input" name="phone" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Re-enter Password</label>
                        <input type="password" class="form-control" name="rePass" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Update Role
            </button>
            </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                <form action="../Databases/EditUserRoleAction.php" method="post">
                    <div class="form-group">
                        <label for="sel1">Role:</label>
                        <select class="form-control" name="role">
                            <option value="1">Admin</option>
                            <option value="2">Staff</option>
                            <option value="3">Customer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">User ID</label>
                        <input class="form-control" type="text" placeholder="Default input" name="uid" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>