<?php
    $id = $_SESSION['user_id'];

    $query = "SELECT * FROM MsUser WHERE `user_id` = $id";
    $result = mysqli_query($conn, $query);
    
    $row=$result->fetch_assoc();

?>

<img src=<?php echo $row['user_profile_picture'] ?> class="rounded-circle mx-auto d-block" width="30%">

<form action="../Databases/EditProfileAction.php" method="post" enctype="multipart/form-data">

<div class="form-group">
    <label for="exampleInputEmail1">Username</label>
    <input class="form-control" type="text" placeholder=<?php echo $row['username'] ?> name="username" required>
</div>
<div class="form-group">
    <label for="exampleFormControlTextarea1">Address</label>
    <textarea class="form-control" placeholder=<?php echo $row['user_address'] ?>rows="3" name="address" required></textarea>
</div>
<div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input class="form-control" type="text" placeholder=<?php echo $row['user_phone'] ?> name="phone" required>
</div>
<div class="form-group">
    <div class="custom-control custom-radio">
        <label for="exampleInputEmail1">Gender</label><br>
        <input type="radio" name="gender" id="female"<?php if (isset($gender) && $gender=="female")?> value="female" checked>Female
        <input type="radio" name="gender" id="male"<?php if (isset($gender) && $gender=="male")  ?> value="male">Male
    </div>
</div>
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" aria-describedby="emailHelp" name="email" required>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changeProfilePictureModal">
    Change Profile Picture
</button>

<!-- Modal -->
<div class="modal fade" id="changeProfilePictureModal" tabindex="-1" role="dialog" aria-labelledby="changeProfilePictureModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Profile Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="changeProfPic">Input Profile Picture</label>
                    <input type="file" class="form-control-file" name="profile">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>  
            </div>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary">Update</button>

</form>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Change Password
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../Databases/EditPasswordAction.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Current Password</label>
                        <input type="password" class="form-control" name="currPass" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">New Password</label>
                        <input type="password" class="form-control" name="newPass" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Re-enter Password</label>
                        <input type="password" class="form-control" name="reNewPass" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                </form>     
            </div>
        </div>
    </div>
</div>